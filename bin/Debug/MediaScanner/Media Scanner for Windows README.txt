

GnMediaScanner Version 1.4.1

Usage:  gnmediascanner.exe [options] <mediaFile>

     Options:

       Credentials:
        -c    <filename>   filename of credentials [default ./gnclient.txt]
                           format of credential file:
                               CLIENTID=<clientid>
                               CLIENTTAG=<clienttag>
        OR
        -cid  <clientid>   ClientID (value provided by Gracenote)
        -ctag <clienttag>  ClientTag (value provided by Gracenote)

       Output:
        -q                 quiet (no status output)
        -nojson            no JSON output
        -nomap             no result map output
        -mapcols <num>     number of columns for result map [default 80]

       Fingerprinting:
        -3s                generate 3 second fingerprints [default]
        -6s                generate 6 second fingerprints
        -9s                generate 9 second fingerprints
        -i <seconds>       fingerprint interval - delay <seconds> of audio betwe
en fingerprints [default 0 (continuous)]
        -s <seconds>       start fingerprints after <seconds> of audio [default
0 (start at beginning)]
        -e <seconds>       end fingerprints after <seconds> of audio [default 0
(end of audio)]

       Audio Quality:
        -vhq               mediaFile is 'very high quality' audio [default]
        -hq                mediaFile is 'high quality' audio
        -mq                mediaFile is 'medium quality' audio
        -lq                mediaFile is 'low quality' audio
        -vlq               mediaFile is 'very low quality' audio