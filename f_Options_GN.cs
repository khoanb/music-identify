﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusicIdentification
{
    public partial class f_Options_GN : Form
    {
        private f_Main main;
        public f_Options_GN(f_Main main1,string command)
        {
            InitializeComponent();
            main = main1;
            textBox1.Text = command;
        }

        private void f_Options_GN_Load(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            main.command = textBox1.Text;
            this.Close();
        }
    }
}
