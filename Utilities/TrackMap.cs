﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MusicIdentification.Utilities
{
    public class TrackMap
    {
        public string title { get; set; }
        public string artist { get; set; }
        public string gnid { get; set; }
        public string song_duration { get; set; }
        public List<TrackPosition> match_positions { get; set; }
        public string match_coverage { get; set; }
        public string match_percentage { get; set; }

        [JsonIgnore]
        public int match_count
        {
            get
            {
                if (match_positions != null)
                    return match_positions.Count();
                return 0;
            }
        }
        
        [JsonIgnore]
        public int totaloccurrences { get; set; }
    }
}
