﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GracenoteSDK;

namespace MusicIdentification.Utilities
{
    public static class Utils
    {
        public static string ReplaceSpecialCharacter(this string text)
        {
            return Regex.Replace(text, "[^0-9a-zA-Z]+", "");
        }

        public static GnFingerprintType GetFingerprintType(int type)
        {
            var result = GnFingerprintType.kFingerprintTypeInvalid;
            switch (type)
            {
                case (int)GnFingerprintType.kFingerprintTypeInvalid:
                    result = GnFingerprintType.kFingerprintTypeInvalid;
                    break;
                case (int)GnFingerprintType.kFingerprintTypeFile:
                    result = GnFingerprintType.kFingerprintTypeFile;
                    break;
                case (int)GnFingerprintType.kFingerprintTypeStream3:
                    result = GnFingerprintType.kFingerprintTypeStream3;
                    break;
                case (int)GnFingerprintType.kFingerprintTypeStream6:
                    result = GnFingerprintType.kFingerprintTypeStream6;
                    break;
                case (int)GnFingerprintType.kFingerprintTypeCMX:
                    result = GnFingerprintType.kFingerprintTypeCMX;
                    break;
                case (int)GnFingerprintType.kFingerprintTypeGNFPX:
                    result = GnFingerprintType.kFingerprintTypeGNFPX;
                    break;
                default:
                    break;
            }
            return result;
        }
        //public static int GetFingerprintInteger(int type)
        //{
        //    var result = 3;
        //    switch (type)
        //    {
        //        case (int)FingerprintEnum.File:
        //            result = 9;
        //            break;
        //        case (int)FingerprintEnum.ThreeSeconds:
        //            result = 3;
        //            break;
        //        case (int)FingerprintEnum.SixSeconds:
        //            result = 6;
        //            break;
        //        default:
        //            break;
        //    }
        //    return result;
        //}
    }
}
