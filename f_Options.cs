﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MusicIdentification.Utilities;
using GracenoteSDK;

namespace MusicIdentification
{
    public partial class f_Options : Form
    {
        private f_Main main;
        public f_Options(f_Main main, int numericUpDown1, int cbbFingerLength,int typeView)
        {
            InitializeComponent();
            this.main = main;
            this.numericUpDown1.Value = numericUpDown1;
            this.cbbFingerLength.ValueMember = "Key";
            this.cbbFingerLength.DisplayMember = "Value";
            this.cbbFingerLength.Items.Add(new KeyValuePair<int, string>((int)GnFingerprintType.kFingerprintTypeInvalid, "kFingerprintTypeInvalid"));
            this.cbbFingerLength.Items.Add(new KeyValuePair<int, string>((int)GnFingerprintType.kFingerprintTypeFile, "kFingerprintTypeFile"));
            this.cbbFingerLength.Items.Add(new KeyValuePair<int, string>((int)GnFingerprintType.kFingerprintTypeStream3, "kFingerprintTypeStream3"));
            this.cbbFingerLength.Items.Add(new KeyValuePair<int, string>((int)GnFingerprintType.kFingerprintTypeStream6, "kFingerprintTypeStream6"));
            this.cbbFingerLength.Items.Add(new KeyValuePair<int, string>((int)GnFingerprintType.kFingerprintTypeCMX, "kFingerprintTypeCMX"));
            this.cbbFingerLength.Items.Add(new KeyValuePair<int, string>((int)GnFingerprintType.kFingerprintTypeGNFPX, "kFingerprintTypeGNFPX"));
            this.cbbFingerLength.SelectedIndex = cbbFingerLength;
            if (typeView == 1)
                radioButton1.Checked = true;
            else
            {
                radioButton2.Checked = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            main.numericUpDown1 = (int)numericUpDown1.Value;
            main.cbbFingerLength = cbbFingerLength.SelectedIndex;
            main.typeView = 1; // 1- all, 2 - first
            if (radioButton2.Checked)
                main.typeView = 2;
            this.Close();
        }

        private void f_Options_Load(object sender, EventArgs e)
        {
            
            //cbbFingerLength.SelectedIndex = 1;
        }
    }
}
