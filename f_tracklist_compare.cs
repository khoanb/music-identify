﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MusicIdentification.Utilities;
using Newtonsoft.Json;

namespace MusicIdentification
{
    public partial class f_tracklist_compare : Form
    {
        List<string> listTrackMatched = new List<string>();
        private string richTextBox1 = "", rtb_json_gn = "";
        List<MusicMap> listTrackByMediaScanner = new List<MusicMap>();
        public f_tracklist_compare()
        {
            InitializeComponent();
        }

        public f_tracklist_compare(List<string> listTrackMatched, string richTextBox1, string rtb_json_gn)
        {
            InitializeComponent();
            this.listTrackMatched = listTrackMatched;
            this.richTextBox1 = richTextBox1;
            this.rtb_json_gn = rtb_json_gn;
        }

        private void f_tracklist_compare_Load(object sender, EventArgs e)
        {
            try
            {
                bindListTrack();
                Bind_richTextBox1();
                bindListTrackResult();
                binddataGridView1();
                binddataGridView2();
                binddataGridView3();
                //bindListTrackMediaScanner();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void bindListTrack()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("stt");
            dt.Columns.Add("name");
            var sid = 1;
            foreach (var item in listTrackMatched)
            {
                DataRow row = dt.NewRow();
                row["stt"] = sid;
                sid++;
                row["name"] = item;
                dt.Rows.Add(row);
            }

            dataGridView_ListTrack.DataSource = dt;
            dataGridView_ListTrack.Columns[0].Width = 108;
            dataGridView_ListTrack.Columns[1].Width = 500;
        }
        private void binddataGridView1()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("stt");
            dt.Columns.Add("name");
            var sid = 1;
            foreach (var item in listTrackMatched)
            {
                DataRow row = dt.NewRow();
                row["stt"] = sid;
                sid++;
                row["name"] = item;
                dt.Rows.Add(row);
            }

            dataGridView1.DataSource = dt;
            dataGridView1.Columns[0].Width = 108;
            dataGridView1.Columns[1].Width = 500;
        }
        private void binddataGridView2()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("stt");
            dt.Columns.Add("title");
            dt.Columns.Add("artist");
            dt.Columns.Add("song_duration");
            dt.Columns.Add("match_coverage");
            dt.Columns.Add("match_percentage");
            dt.Columns.Add("match_count");

            var sid = 1;
            var lst = JsonConvert.DeserializeObject<MusicMap>(rtb_json_gn);
            if (lst.matches != null)
            {
                lst.matches = lst.matches.OrderBy(x => x.title).ToList();
                foreach (var item in lst.matches)
                {
                    DataRow row = dt.NewRow();
                    row["stt"] = sid;
                    sid++;
                    row["title"] = item.title;
                    row["artist"] = item.artist;
                    row["song_duration"] = item.song_duration;
                    row["match_coverage"] = item.match_coverage;
                    row["match_percentage"] = item.match_percentage;
                    row["match_count"] = item.match_count;
                    dt.Rows.Add(row);
                }


            }
            dataGridView2.DataSource = dt;
            dataGridView2.Columns[0].Width = 50;
            dataGridView2.Columns[1].Width = 300;
        }
        private void binddataGridView3()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("stt");
            dt.Columns.Add("title");
            dt.Columns.Add("artist");
            dt.Columns.Add("match_count");
            var sid = 1;
            var lstItem = new Dictionary<int, string>();
            var lst = JsonConvert.DeserializeObject<MusicMap>(rtb_json_gn);
            if (lst.matches != null)
            {
                lst.matches = lst.matches.OrderBy(x => x.title).ToList();
                //foreach (var item in listTrackMatched)
                //{
                foreach (var item2 in lst.matches)
                {
                    if (item2.match_count >= numericUpDown1.Value)
                    {
                        DataRow row = dt.NewRow();
                        row["stt"] = sid;
                        row["title"] = item2.title;
                        row["artist"] = item2.artist;
                        row["match_count"] = item2.match_count;
                        dt.Rows.Add(row);
                        sid++;
                    }
                }
                //}

            }

            dataGridView3.DataSource = dt;
            dataGridView3.Columns[0].Width = 108;
            dataGridView3.Columns[1].Width = 300;
        }
        private void Bind_richTextBox1()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("stt");
            dt.Columns.Add("title");
            dt.Columns.Add("artist");
            dt.Columns.Add("song_duration");
            dt.Columns.Add("match_coverage");
            dt.Columns.Add("match_percentage");
            dt.Columns.Add("match_count");

            var sid = 1;
            listTrackByMediaScanner.Clear();
            var lst = JsonConvert.DeserializeObject<List<TrackMap>>(richTextBox1);
            lst = lst.OrderBy(x => x.title).ToList();
            var totalSongs = lst.Select(x => x.match_positions).Sum(x => x.Count);

            foreach (var item in lst)
            {
                float percent = (float)(item.match_positions.Count() * 100) / totalSongs;

                DataRow row = dt.NewRow();
                row["stt"] = sid;
                sid++;
                row["title"] = item.title;
                row["artist"] = item.artist;
                row["song_duration"] = item.song_duration;
                row["match_coverage"] = item.match_coverage;
                row["match_percentage"] = string.Format("{0:N4}", percent) + " %";
                row["match_count"] = item.match_positions.Count();
                dt.Rows.Add(row);
            }

            dataGridView_ListFound.DataSource = dt;
            dataGridView_ListFound.Columns[0].Width = 50;
            dataGridView_ListFound.Columns[1].Width = 300;

        }
        private void bindListTrackResult()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("stt");
            dt.Columns.Add("title");
            dt.Columns.Add("artist");
            // dt.Columns.Add("match_percentage");
            dt.Columns.Add("match_count");
            var sid = 1;
            var lst = JsonConvert.DeserializeObject<List<TrackMap>>(richTextBox1);


            lst = lst.OrderBy(x => x.title).ToList();
            //foreach (var item in listTrackMatched)
            //{
            foreach (var item2 in lst)
            {
                if (item2.match_count >= numericUpDown1.Value)
                {
                    DataRow row = dt.NewRow();
                    row["stt"] = sid;
                    row["title"] = item2.title;
                    row["artist"] = item2.artist;
                    //row["match_percentage"] = item2.match_percentage;
                    row["match_count"] = item2.match_count;
                    dt.Rows.Add(row);
                    sid++;
                }
                //}
            }

            dataGridView_ListResult.DataSource = dt;
            dataGridView_ListResult.Columns[0].Width = 108;
            dataGridView_ListResult.Columns[1].Width = 300;
        }
        private void bindListTrackMediaScanner()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("stt");
            dt.Columns.Add("name");
            var sid = 1;
            foreach (var item in listTrackByMediaScanner)
            {
                DataRow row = dt.NewRow();
                row["stt"] = sid;
                sid++;
                row["name"] = item;
                dt.Rows.Add(row);
            }
            //dtListMediaScanner.DataSource = dt;
            //dtListMediaScanner.Columns[0].Width = 108;
            //dtListMediaScanner.Columns[1].Width = 500;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            bindListTrackResult();
            binddataGridView3();
        }
    }
}
